
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class NeuvooApplication {


    public static void main(String[] args) throws ClassNotFoundException, SQLException {
      	WebDriver driver = new FirefoxDriver();
		
        String baseUrl = "https://neuvoo.ca/advertiser/?lang=en";

        // launch Fire fox and direct it to the Base URL
        driver.get(baseUrl);
        
        
        
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("jason@jasonstevens.ca");
        driver.findElement(By.id("login")).clear();
        driver.findElement(By.id("login")).sendKeys("fbg2017");
        driver.findElement(By.cssSelector("input.button-s")).click();
        
        
        WebElement table = driver.findElement(By.id("sjc_table"));
        List<WebElement> allRows = table.findElements(By.tagName("tr"));
        
        String val = "";
	    ArrayList<String> valList = new ArrayList<String>();
	    
		for (WebElement row : allRows) {
			List<WebElement> cells = row.findElements(By.tagName("td"));
			val = "";
			for (WebElement cell : cells) {
				val = val+"|"+cell.getText();
			}
			
			if(!val.trim().equals("")){
				val = val.substring(1);				
				valList.add(val);
			}
			
		}
		
		driver.close();
		
		ArrayList<String> dateList = com.neuvoo.handler.NeuvooHandler.getDateList();
		
		String date = "";
		int paidClicks = 0;
		int organicClicks = 0;
		double avgCpc = 0.0;
		float cost = 0;
		String[] tabelVals = null;
		String qryvals = "";
		for(String x : valList){

			tabelVals = x.split("\\|");
			
			if(tabelVals[0].trim().equals("")){ continue; }
			
			date = tabelVals[0];
			paidClicks = Integer.parseInt(tabelVals[1].replace(",", ""));
			avgCpc = Double.parseDouble(tabelVals[3].replace(" (USD)", ""));
			cost = Float.parseFloat(tabelVals[4].replace(" (USD)", ""));
			
			//Date check, only insert new record
	        if(dateList.contains(date)){ continue; }
	        
	     	//Current Date Check
	        if(date.equals(com.neuvoo.util.Utility.currentDate())){ continue; }
	        
	        //Valid Check
	        if(date.contains("Total")){ continue; }
	        
	        //Insert into DB
			System.out.println("date: "+date);
			System.out.println("paidClicks: "+paidClicks);
			System.out.println("avgCpc: "+avgCpc);
			System.out.println("cost: "+cost);
			qryvals = qryvals+","+"(null, "+paidClicks+", "+cost+", 0, 0, 'Neuvoo-jobExport', '"+date+"' )";
			System.out.println("----------------------------------");
			
			
		}
		
		//Insert into DB
		String qry = "";
		if(!qryvals.trim().equals("")){
			qryvals = qryvals.substring(1);
			Statement st = com.neuvoo.connection.Config.Connect().createStatement();
			qry = "INSERT INTO tbl_job_export_cost_stats "
					+ "(id, clicks, cost, impressions, jobs_count, export_source, date) "
					+ "VALUES "+qryvals;
			System.out.println(qry);
			
			st.executeUpdate(qry);
			com.neuvoo.connection.Config.Close();
		}
		
		System.out.println("Done");
        
        
        
       
    }

}