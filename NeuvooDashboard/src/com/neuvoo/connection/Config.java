package com.neuvoo.connection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Config {
    
    private static Connection con;
    
    public static Connection Connect() throws ClassNotFoundException, SQLException{
    	
    	if(con!=null && !con.isClosed())
    		return con;
    				
        Class.forName("com.mysql.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mysql://oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306/oakalerts","awsoakuser","awsoakusersignity");        
        return con;
    }
    
    public static void Close() throws SQLException{
        con.close(); 
    }
    
}
