package com.neuvoo.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {
	
	public static int getCurrMonth(){
		
		DateFormat dateFormat = new SimpleDateFormat("MM");
		Date date = new Date();
		int month = Integer.parseInt(dateFormat.format(date));
		
		return month;
	}


	public static String getYesterdayDate() {
	    final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
	    return s.format(new Date(cal.getTimeInMillis()));
    }
	
	
	public static String convertDate(String date1) throws ParseException{
		String date2 = "";		

		if(date1.contains("/")){	
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Date d = sdf.parse(date1);
			sdf.applyPattern("yyyy-MM-dd");
			date2 = sdf.format(d);
		}else{
			date2 = date1;
		}
		
		return date2;
	}
	
	public static String currentDate(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		return dateFormat.format(date);
	}

	
	public static void main(String[] args) throws ParseException {
		System.out.println(convertDate("9/2/2017"));		
	}
	
	
}
