package com.neuvoo.handler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class NeuvooHandler {
	
	public static ArrayList<String> getDateList() throws ClassNotFoundException, SQLException{
		ArrayList<String> dataList = new ArrayList<String>();

		Statement st = com.neuvoo.connection.Config.Connect().createStatement();
		ResultSet rs = st.executeQuery("select distinct date from tbl_job_export_cost_stats where export_source='Neuvoo-jobExport';");		
		while(rs.next()){	
			if(rs.getString("date")==null || rs.getString("date").trim().equals("")){ continue; }
			
			dataList.add(rs.getString("date"));
		}
		
		com.neuvoo.connection.Config.Close();
		
		return dataList;
	}
	
	

}
